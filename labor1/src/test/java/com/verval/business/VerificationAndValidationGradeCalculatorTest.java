package com.verval.business;

import com.verval.business.business.VerificationAndValidationGradeCalculator;
import com.verval.business.exception.InvalidPointException;
import com.verval.business.model.GradeType;
import com.verval.business.model.Point;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.sql.SQLOutput;
import java.util.ArrayList;

public class VerificationAndValidationGradeCalculatorTest {

    private VerificationAndValidationGradeCalculator calculator;

    @BeforeAll
    static void testSetupSteps() {
        System.out.println("Some setup steps here");

    }

    @AfterAll
    static void tearDown() {
        System.out.println("Tidy up after tests");

    }

    @BeforeEach
    void eachSeparateTestPreparationSteps() {
        System.out.println("Stuff I want to happen before each scenario");

        calculator = new VerificationAndValidationGradeCalculator();
    }

    @ParameterizedTest(name = "Invalid grades")
    @CsvSource({
            "19",
            "10.1",
            "-0.1",
            "-4",
    })
    void invalidPoints(double grade) {
        Assertions.assertThrows(InvalidPointException.class, () -> calculator.addPoint(new Point(grade, 10, GradeType.Lecture)), grade + " hould be invalid grade");
    }

    @Test
    @DisplayName("Add 2 points")
    void add2PointsToCalculator() {
        calculator.addPoint(new Point(3, 10, GradeType.Seminar));
        calculator.addPoint(new Point(5, 10, GradeType.Laboratory));

        Assertions.assertEquals(3, calculator.getPoints().size(), "Calculator should contain 3 points");
    }

    @Test
    void passingGrade() {
        calculator.addPoint(new Point(6, 10, GradeType.Seminar));
        calculator.addPoint(new Point(9, 10, GradeType.Seminar));

        calculator.addPoint(new Point(9, 10, GradeType.Laboratory));
        calculator.addPoint(new Point(4, 10, GradeType.Laboratory));

        calculator.addPoint(new Point(8, 10, GradeType.Lecture));
        calculator.addPoint(new Point(10, 10, GradeType.Lecture));

        calculator.addPoint(new Point(6, 10, GradeType.Exam));

        Assertions.assertTrue(calculator.isPassing(), "Shold have a passing grade");
    }

    @Test
    void correctPointsArray() {
        Point point1 = new Point(4, 10, GradeType.Exam);
        Point point2 = new Point(5, 10, GradeType.Lecture);
        Point point3 = new Point(2, 10, GradeType.Laboratory);
        Point point4 = new Point(7, 10, GradeType.Granted);
        Point point5 = new Point(9, 10, GradeType.Seminar);

        calculator.addPoint(point1);
        calculator.addPoint(point2);
        calculator.addPoint(point3);
        calculator.addPoint(point4);
        calculator.addPoint(point5);

        double[] actual = new double[calculator.getPoints().size()];
        int i = 0;
        for (Point p : calculator.getPoints()) {
            actual[i++] = p.getPoint();
        }

        Assertions.assertArrayEquals(new double[]{10, 4, 5, 2, 7, 9}, actual);
    }

    @ParameterizedTest(name = "year of study")
    @CsvSource({
            "true, 0x7fffffff",
            "false, 0" ,
    })
    void unsetStudyYear(Boolean exceptional, int expected){
        int year = calculator.getYearOfStudy(exceptional);

        Assertions.assertEquals(expected, year);
    }
}
