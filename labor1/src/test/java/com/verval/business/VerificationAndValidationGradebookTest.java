package com.verval.business;

import com.verval.business.business.VerificationAndValidationGradeCalculator;
import com.verval.business.business.VerificationAndValidationGradebook;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import static org.mockito.ArgumentMatchers.*;


public class VerificationAndValidationGradebookTest {

    private VerificationAndValidationGradebook gradebookSpy;

    @Mock
    private VerificationAndValidationGradebook gradebookMock;

    @Spy
    VerificationAndValidationGradeCalculator calculatorSpy;

    @Mock
    private VerificationAndValidationGradeCalculator calculatorMock;

    @Captor
    private ArgumentCaptor<Integer> argumentCaptor;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        gradebookSpy = Mockito.spy(new VerificationAndValidationGradebook(3));
    }


    @Test
    void whenAddingExceptionStudent_thenItIsAdded(){
        calculatorMock = new VerificationAndValidationGradeCalculator();
        gradebookSpy.addvAndVGradeCalculator(calculatorMock, true);

        Mockito.verify(gradebookSpy).addvAndVGradeCalculator(calculatorMock, true);
        Assertions.assertEquals(1, gradebookSpy.getvAndVGradeCalculators().size());
    }

    @Test
    void whenGetAverageOfGrades_thenAverageGiven(){
        Mockito.doReturn(8.2).when(calculatorMock).getGrade();
        Mockito.when(calculatorMock.getYearOfStudy(anyBoolean())).thenReturn(Integer.MAX_VALUE);
        gradebookSpy.addvAndVGradeCalculator(calculatorMock, false);
        Assertions.assertEquals(8.2, gradebookSpy.getAverageOfGrades().getAsDouble());
    }

    @Test
    void whenSetMinimumYearOfStudyForCourse_thenYearIsSet(){
        gradebookMock.setMinimumYearOfStudyForCourse(10);
        Mockito.verify(gradebookMock).setMinimumYearOfStudyForCourse(argumentCaptor.capture());

        Assertions.assertEquals(10, argumentCaptor.getValue() );
    }

    @Test
    void whenGetCountOfPassingGrades_thenCountIsGiven(){
        Mockito.doReturn(true).when(calculatorMock).isPassing();
        Mockito.when(calculatorMock.getYearOfStudy(anyBoolean())).thenReturn(Integer.MAX_VALUE);

        gradebookSpy.addvAndVGradeCalculator(calculatorMock, false);
        gradebookSpy.addvAndVGradeCalculator(calculatorMock, true);
        gradebookSpy.addvAndVGradeCalculator(calculatorMock, false);

        Assertions.assertEquals(3, gradebookSpy.getCountOfPassingGrades());
    }
}
