package com.verval.business.exception;

public class NotEligibleStudentException extends RuntimeException {
    public NotEligibleStudentException(String errorMessage) {
        super(errorMessage);
    }
}
